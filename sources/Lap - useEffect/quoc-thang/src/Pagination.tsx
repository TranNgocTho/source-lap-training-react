type PaginationInfo = {
    _page: number,
    _limit: number,
    _totalRows: number
}

type Props = {
    pagination: PaginationInfo,
    onPageChange: any,
    isFirst: boolean,
    isLast: boolean
}

function Pagination(props: Props) {
    const { pagination, onPageChange, isFirst, isLast } = props;
    return (
        <>
        <button disabled={isFirst} onClick={()=>onPageChange(pagination._page - 1)}>Prev</button>
        <button disabled={isLast} onClick={()=>onPageChange(pagination._page + 1)}>Next</button>
        </>        
    )
}

export default Pagination;