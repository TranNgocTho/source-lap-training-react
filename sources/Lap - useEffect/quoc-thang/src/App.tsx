import './App.css';
import { useEffect, useState } from 'react';
import PostList  from './PostList'
import Pagination from './Pagination';

function App() {
  const [postList, setPostList] = useState([]);
  const [pagination, setPagination] = useState({
    _page: 1,
    _limit: 10,
    _totalRows: 1
  });
  const [isFirst, setIsFirst] = useState(true);
  const [isLast, setIsLast] = useState(false);
  const [maxPage, setMaxPage] = useState(0);
  async function fetchData() {
    try {
      const requestUrl = "http://js-post-api.herokuapp.com/api/posts?_limit=10&_page=" + pagination._page;
      const response = await fetch(requestUrl);
      const responseJson = await response.json();
      setPostList(responseJson.data);
      var totalRows = responseJson.pagination._totalRows;
      var limitRow = responseJson.pagination._limit;
      // Calculate maxPage
      var maxCalculatedPage = 0;
      if(totalRows % limitRow === 0){
        maxCalculatedPage = Math.round(totalRows / limitRow);
      }else {
        maxCalculatedPage = Math.round(totalRows / limitRow) + 1;
      }
      setMaxPage(maxCalculatedPage);
    }catch(error){
      console.log('Failed to fetch data');
    }
  }
  useEffect(() => {
    fetchData();
  },[pagination])

  function handlePageChange(newPage: number) {
    setPagination({...pagination, _page: newPage});
    if (newPage === 1) {
      setIsFirst(true);
    }else {
      setIsFirst(false);
    }
    if(newPage === maxPage){
      setIsLast(true);
    }else{
      setIsLast(false);
    }
  }
  
  return (
    <>
    <PostList list={postList} />
    <Pagination pagination={pagination} onPageChange={handlePageChange} isFirst={isFirst} isLast={isLast} />
    </>    
  );
}

export default App;