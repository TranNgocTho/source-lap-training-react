type Props = {
    list: any[];
}

function PostList(props: Props) {
    const { list } = props;
    return (
        <ul>
            {
                list.map((item) => {
                    return <li>{ item.title }</li>
                })
            }
        </ul>
    )
}

export default PostList;