import List from "@mui/material/List";
import Post, { IPost } from "./Post";

// const data: IPost[] = [
//   {
//     id: "ifasfafd",
//     title: "title",
//     author: "author",
//     description: "description",
//     imageUrl: "https://picsum.photos/id/646/1368/400",
//   },
//   {
//     id: "ifd",
//     title: "Hic",
//     author: "author",
//     description:
//       "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Maxime libero quisquam quia earum facere atque. Inventore, perferendis architecto et nam placeat velit ipsam recusandae vel perspiciatis temporibus adipisci. In, voluptates!",
//     imageUrl: "https://picsum.photos/id/646/1368/400",
//   },
//   {
//     id: "idsfafd",
//     title: "title",
//     author: "author",
//     description: "description",
//     imageUrl: "https://picsum.photos/id/646/1368/400",
//   },
// ];

type Props = {
  posts: IPost[];
};

export default function ListPost({ posts }: Props) {
  return (
    <List sx={{ width: "100%", maxWidth: 700, bgcolor: "background.paper" }}>
      {posts.map((post) => (
        <Post key={post.id} post={post} />
      ))}
    </List>
  );
}
