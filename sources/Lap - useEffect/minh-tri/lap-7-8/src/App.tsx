import { useEffect, useState } from "react";
import "./App.css";
import ListPost from "./components/PostList";
import { Pagination } from "@mui/material";
import axios from "axios";
import { IPost } from "./components/Post";

const LIMIT: number = 10;

function App() {
  const [posts, setPosts] = useState<IPost[]>();
  const [totalPage, setTotalPage] = useState<number>();
  const [curPage, setCurPage] = useState<number>();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    const fetchPost = async (page: number) => {
      try {
        setLoading(true);
        const { data } = await axios.get(
          `http://js-post-api.herokuapp.com/api/posts?_limit=10&_page=${page}`
        );
        setTotalPage(Math.ceil(data.pagination._totalRows / LIMIT));
        setPosts(data.data);
        setLoading(false);
        setError(false);
      } catch (error) {
        setLoading(false);
        setError(true);
      }
    };
    fetchPost(curPage || 1);
  }, [curPage]);

  const onPageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setCurPage(value);
  };

  return (
    <div className="App">
      {loading ? (
        <h3>Loading ... 🥱🥱🥱</h3>
      ) : error ? (
        <h3>An error occured while loading posts 😥😥😥!</h3>
      ) : (
        <ListPost posts={posts!} />
      )}

      <Pagination
        count={totalPage}
        color="secondary"
        onChange={onPageChange}
        showFirstButton
        showLastButton
      />
    </div>
  );
}

//

export default App;
