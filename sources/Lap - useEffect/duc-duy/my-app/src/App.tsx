import React, { useEffect, useState } from "react";
import "./App.css";
import PostList from "./postList/PostList";
import Pagination from "./postList/Pagination";

const initPost = {
  id: "",
  title: "",
  author: "",
  description: "",
  createdAt: "",
  updatedAt: "",
  imageUrl: "",
};

const initPagination = {
  _limit: 10,
  _page: 1,
  _totalRows: 44,
};

function App() {
  const [postsList, setPostsList] = useState([initPost]);
  const [pagination, setPagination] = useState(initPagination);

  useEffect(() => {
    async function fetchData() {
      await fetch(
        `http://js-post-api.herokuapp.com/api/posts?_limit=${pagination._limit}&_page=${pagination._page}`
      )
        .then((res) => res.json())
        .then(
          (result) => {
            setPostsList(result.data);
            if (pagination._totalRows !== result.pagination._totalRows) {
              setPagination({
                ...pagination,
                _totalRows: result.pagination._totalRows,
              });
            }
          },
          (error) => {
            console.log("Failed to fetch posts: ", error);
          }
        );
    }
    fetchData();
  }, [pagination]);

  function handlePageChange(newPage: number) {
    setPagination({ ...pagination, _page: pagination._page + newPage });
  }

  return (
    <div style={{ margin: 20 }}>
      <PostList list={postsList} />
      <Pagination onPageChange={handlePageChange} pagination={pagination} />
    </div>
  );
}

export default App;
