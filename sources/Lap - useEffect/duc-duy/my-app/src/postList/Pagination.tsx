import React from "react";

type Props = {
  onPageChange: (newpage: number) => void;
  pagination: { _limit: number; _page: number; _totalRows: number };
};

function Pagination(props: Props) {

    
  const handlePrev = () => {
    props.onPageChange(-1);
  };
  const handleNext = () => {
    props.onPageChange(1);
  };


  return (
    <div style={{ margin: 20 }}>
      <button
        disabled={props.pagination._page <= 1 ? true : false}
        onClick={handlePrev}
      >
        Prev
      </button>
      <input
        type="text"
        value={props.pagination._page}
        style={{ width: 20 }}
        disabled
      />
      <button  disabled={ (props.pagination._totalRows/props.pagination._limit <= props.pagination._page ) ? true : false} onClick={handleNext}>Next</button>
    </div>
  );
}

export default Pagination;
