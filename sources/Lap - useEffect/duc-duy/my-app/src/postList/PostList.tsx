import React from "react";

type Prop = {
  list: any;
};

function PostList(props: Prop) {
  const { list } = props;

  return (
    <ul>
      {list.map((item: any) => (
        <li key={item.id}>{item.title}</li>
      ))}
    </ul>
  );
}

export default PostList;
