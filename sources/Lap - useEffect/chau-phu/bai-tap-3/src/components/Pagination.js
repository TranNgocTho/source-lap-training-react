import Button from 'react-bootstrap/Button';

function Pagination({ pagination, currentPage, onPageChange, onPreviousPage, onNextPage }) {

    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(pagination._totalRows / pagination._limit); i++) {
        pageNumbers.push(i);
    }

    function paginate(pageNumber) {
        console.log("pageNumber: " + pageNumber);
        onPageChange(pageNumber);
    }

    const previousPage = () => {
        if (currentPage !== 1) {
            onPreviousPage();
        }
    };

    const nextPage = () => {
        if (currentPage !== pageNumbers.length) {
            onNextPage();
        }
    };

    return (
        <div className="pagination-container">

            <Button disabled={currentPage === 1} as="a" variant="primary"
                onClick={() => previousPage()}>
                Previous
            </Button>

            {pageNumbers.map((number) => (
                <button key={number} className={currentPage === number ? 'active' : ''}
                    onClick={() => paginate(number)} >
                    {number}
                </button>
            ))}

            <Button disabled={currentPage === pageNumbers.length} as="a" variant="primary"
                onClick={() => nextPage()}>
                Next
            </Button>

        </div>
    )
};

export default Pagination;