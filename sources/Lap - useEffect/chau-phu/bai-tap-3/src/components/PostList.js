import React, { useEffect, useState } from "react";
import Pagination from "./Pagination";

function PostList(posts) {

    const [postList, setPostList] = useState('');
    const [page, setPage] = useState(1);
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1
    });

    // async function fetchData() {
    //     try {
    //         const requestUrl = `http://js-post-api.herokuapp.com/api/posts?_limit=10&_page=${page}`;
    //         const response = await fetch(requestUrl);
    //         const responseJSON = await response.json();
    //         const { data, pagination } = responseJSON;
    //         // console.log("queryParamsString", queryParamsString);
    //         console.log({ data, pagination });
    //         setPostList(data);
    //         setPagination(pagination);
    //     } catch (error) {
    //         console.log('Failed to fetch posts: ', error.message);
    //     }
    // }

    useEffect(() => {
        async function fetchData() {
            try {
                // TODO: Should split into a separated api file instead of using fetch directly
                // const queryParamsString = queryString.stringify();
                const requestUrl = `http://js-post-api.herokuapp.com/api/posts?_limit=10&_page=${page}`;
                const response = await fetch(requestUrl);
                const responseJSON = await response.json();
                const { data, pagination } = responseJSON;

                console.log({ data, pagination });
                setPostList(data);
                setPagination(pagination);
            } catch (error) {
                console.log('Failed to fetch posts: ', error.message);
            }
        };
        fetchData();
    }, [page]);

    function handlePageChange(newPage) {
        // Call API to re-fetch data with newPage
        setPage(newPage);
    }

    function handlePrevPage() {
        setPage(page => page - 1);
    }

    function handleNextPage() {        
        setPage(page => page + 1);
    }

    return (
        <div>
            <ul>
                {
                    postList && postList.length > 0 && postList.map((item, index) => <li obj={item} key={index}>{item.title}</li>)
                }
            </ul>
            <div>
                {
                    <Pagination
                        pagination={pagination}
                        currentPage={page}
                        onPageChange={handlePageChange}
                        onPreviousPage={handlePrevPage}
                        onNextPage={handleNextPage}></Pagination>
                }

            </div>
        </div>
    )
}

export default PostList;