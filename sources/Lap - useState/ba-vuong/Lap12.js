import { useState } from "react"

function Lap12(){

    const [color, setColor] = useState(localStorage.getItem("color") || 'deeppink')
    const colors = ["green", "yellow", "black", "blue", "red", "orange", "purple", "pink", "brown", "gray", "white", "cyan", "magenta", "olive", "indigo"];

    const handleChangeColor = () => {
        const index = Math.floor(Math.random() * colors.length);
        setColor(colors[index])
        localStorage.setItem("color", colors[index]);
    }

    const todoList = [
          { id: 1, title: "I love Easy Frontend! 😍 " },
          { id: 2, title: "We love Easy Frontend! 🥰 " },
          { id: 3, title: "They love Easy Frontend! 🚀 " },
    ];

    const [list, setList] = useState(todoList)
        
    const handleRemove = id => {
        setList(old => {
            return old.filter(todo => todo.id !== id)
        })
    }

    return(
        <>
            <h2>Lap 1: Color box</h2>
            <div
                style={{ backgroundColor: color, height: 100, width: 100}}
                onClick={handleChangeColor}
            >
            </div>
            <span>Click into Box to change color</span>
            <h2>---------------------------------</h2>
            <h2>Lap 2: Todolist</h2>
            <ul>
                {list.map((todo) => (
                <li key={todo.id} onClick={() => handleRemove(todo.id)}>{todo.title}</li>
                ))}
            </ul>
            <span>Click Todo item to remove</span>
            <h2>---------------------------------</h2>
        </>
    )
}

export default Lap12