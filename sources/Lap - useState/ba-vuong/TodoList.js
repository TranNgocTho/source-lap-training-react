import React, { useState } from "react";
import "./TodoList.css";

function TodoList() {
  const [items, setItems] = useState(JSON.parse(localStorage.getItem("items")) || []);
  const [inputValue, setInputValue] = useState("");
  const [editIndex, setEditIndex] = useState(null);
  const [editValue, setEditValue] = useState("");

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleAddItem = (e) => {
    e.preventDefault();
    if (inputValue.trim() === "") return;
    setItems((prevItems) => {
        const newItems = [...prevItems, inputValue.trim()]
        const jsonItems = JSON.stringify(newItems);
        localStorage.setItem("items", jsonItems);
        return newItems;
    });
    setInputValue("");
  };

  const handleDeleteItem = (index) => {
    setItems((prevItems) => {
        const deleItem = prevItems.filter((_, i) => i !== index)
        const jsonItems = JSON.stringify(deleItem);
        localStorage.setItem("items", jsonItems);
        return deleItem
    });
    if (editIndex === index) {
      setEditIndex(null);
      setEditValue("");
    }
  };

  const handleEditItem = (index) => {
    setEditIndex(index);
    setEditValue(items[index]);
  };

  const handleEditInputChange = (e) => {
    setEditValue(e.target.value);
  };

  const handleSaveEdit = (index) => {
    if (editValue.trim() === "") return;
    const newItems = [...items];
    newItems[index] = editValue.trim();
    setItems(() => {
        localStorage.setItem("items", JSON.stringify(newItems));
        return newItems;
    });
    setEditIndex(null);
    setEditValue("");
  };

  const handleCancelEdit = () => {
    setEditIndex(null);
    setEditValue("");
  };

  return (
    <div className="todo-list">
      <h1>Todo List</h1>
      <form onSubmit={handleAddItem}>
        <input
          type="text"
          placeholder="Add a new item"
          value={inputValue}
          onChange={handleInputChange}
        />
        <button className="add-btn" type="submit">
          Add
        </button>
      </form>
      <ul>
        {items.map((item, index) => (
          <li key={index}>
            {editIndex === index ? (
              <>
                <input
                  type="text"
                  value={editValue}
                  onChange={handleEditInputChange}
                />
                <div className="btn-group">
                  <button className="save-btn" onClick={() => handleSaveEdit(index)}>Save</button>
                  <button className="cancel-btn" onClick={() => handleCancelEdit()}>Cancel</button>
                </div>
              </>
            ) : (
              <>
                <div className="todo-text">{item}</div>
                <div className="btn-group">
                  <button className="edit-btn" onClick={() => handleEditItem(index)}>Edit</button>
                  <button className="delete-btn" onClick={() => handleDeleteItem(index)}>Delete</button>
                </div>
              </>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TodoList;
