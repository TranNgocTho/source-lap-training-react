type Props = {
  color: string;
  rounded?: boolean;
};

function ColorBox(props: Props) {
  const { color } = props;
  return <div className="box" style={{ backgroundColor: color }}></div>;
}

ColorBox.defaultProps = {
  rounded: true,
};

export default ColorBox;
