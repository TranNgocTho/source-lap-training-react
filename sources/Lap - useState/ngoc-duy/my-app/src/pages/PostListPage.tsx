import queryString from "query-string";
import { useEffect, useState } from "react";
import { Pagination, PaginationModel } from "../features/Pagination/Pagination";
import { PostList } from "../features/PostList/PostList";

function PostListPage() {
  const [postList, setPostList] = useState([]);
  const [pagination, setPagination] = useState<PaginationModel>({
    _page: 1,
    _limit: 10,
    _totalRows: 11,
  });

  const [filters, setFilters] = useState({
    _limit: 10,
    _page: 1,
  });

  useEffect(() => {
    // npm i --save query-string
    async function fetchPostList() {
      try {
        //...
        const paramString = queryString.stringify(filters);
        const requestUrl = `http://js-post-api.herokuapp.com/api/posts?${paramString}`;
        const response = await fetch(requestUrl);
        const responseJson = await response.json();
        console.log({ responseJson });
        const { data, pagination } = responseJson;
        setPostList(data);
        setPagination(pagination);
      } catch (error) {
        console.log(error);
      }
    }

    fetchPostList();
  }, [filters]);

  function handlePageChange(newPage: number) {
    console.log("New page: ", newPage);
    setFilters({
      ...filters,
      _page: newPage,
    });
  }

  return (
    <>
      <PostList posts={postList} />
      <Pagination pagiantion={pagination} onPageChange={handlePageChange} />
    </>
  );
}

export default PostListPage;
