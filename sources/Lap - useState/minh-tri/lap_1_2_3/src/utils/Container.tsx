import styled from "styled-components";

type Props = {
  children: JSX.Element;
};

const Container = ({ children }: Props) => {
  return <ContainerStyle>{children}</ContainerStyle>;
};

export default Container;

const ContainerStyle = styled.div`
  width: 500px;
  margin: auto;
`;
