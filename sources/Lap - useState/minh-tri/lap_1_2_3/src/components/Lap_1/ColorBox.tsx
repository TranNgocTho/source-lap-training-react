import { useEffect, useState } from "react";
import styled from "styled-components";
import Container from "../../utils/Container";

type Props = {
  color: string;
};

const colorsList = ["deeppink", "green", "yellow", "black", "blue"];

const getRandomColor = (curColor: string) => {
  const listRemainColor = colorsList.filter((color) => color !== curColor);
  const randIndex = Math.floor(Math.random() * 4);
  localStorage.setItem("color", listRemainColor[randIndex]);
  return listRemainColor[randIndex];
};

const ColorBox = () => {
  const [color, setColor] = useState<string>(
    localStorage.getItem("color") || "deeppink"
  );

  useEffect(() => {
    localStorage.setItem("color", color);
  }, []);

  return (
    <Container>
      <>
        <Title>Color Box</Title>
        <ColorBoxStyle
          onClick={() => setColor((state) => getRandomColor(state))}
          color={color}
        />
        <h5 style={{ textAlign: "center", color: "gray" }}>
          * Click the box to change color
        </h5>
      </>
    </Container>
  );
};

export default ColorBox;

const ColorBoxStyle = styled.div<Props>`
  margin: auto;
  margin-top: 2rem;
  width: 300px;
  height: 300px;
  background-color: ${(props) => props.color};
`;

const Title = styled.h1`
  display: block;
  text-align: center;
  margin: auto;
  -webkit-background-clip: text;
  color: transparent;
  background: linear-gradient(to right, deeppink, green, yellow, black, blue);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;
