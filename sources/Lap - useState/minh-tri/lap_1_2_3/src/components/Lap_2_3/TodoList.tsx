import { useEffect, useState } from "react";
import { Todo } from "../../models/models";
import Container from "../../utils/Container";
import TodoForm from "./TodoForm";
import TodoItem from "./TodoItem";

const TodoList = () => {
  const [todoList, setTodoList] = useState<Todo[]>(
    JSON.parse(localStorage.getItem("todo-list")!) || []
  );

  const handleAddTodoItem = (title: string) => {
    console.log("title", title);
    setTodoList((prev) => {
      console.log("prev", prev);
      const newTodoList = [...prev];
      newTodoList.push({
        id: new Date().toString(),
        title,
      });
      localStorage.setItem("todo-list", JSON.stringify(newTodoList));
      return newTodoList;
    });
  };

  const handleDeleteTodoItem = (id: string) => {
    setTodoList((prev) => {
      const newTodoList = prev.filter((todo) => todo.id !== id);
      localStorage.setItem("todo-list", JSON.stringify(newTodoList));
      return newTodoList;
    });
  };

  useEffect(() => {
    localStorage.setItem("todo-list", JSON.stringify(todoList));
  }, []);

  console.log("Render");

  const content = todoList.length ? (
    todoList.map((todo) => (
      <TodoItem
        todo={todo}
        handleDeleteTodoItem={handleDeleteTodoItem}
        key={todo.id}
      />
    ))
  ) : (
    <h4
      style={{
        textAlign: "center",
        color: "gray",
      }}
    >
      No item to display
    </h4>
  );

  return (
    <Container>
      <>
        <h1 style={{ display: "block", textAlign: "center", margin: "auto" }}>
          Todo List
        </h1>
        <TodoForm handleAddTodoItem={handleAddTodoItem} />
        {content}
      </>
    </Container>
  );
};

export default TodoList;
