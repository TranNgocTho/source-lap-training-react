import React from "react";
import styled from "styled-components";
import { Todo } from "../../models/models";

type Props = {
  todo: Todo;
  handleDeleteTodoItem: (id: string) => void;
};

const TodoItem = ({ todo, handleDeleteTodoItem }: Props) => {
  return (
    <TodoItemStyle>
      <div>{todo.title}</div>
      <button onClick={() => handleDeleteTodoItem(todo.id)}>x</button>
    </TodoItemStyle>
  );
};

export default TodoItem;

const TodoItemStyle = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 3px;
  padding: 5px;
  background-color: rgb(189, 214, 252);
  border-radius: 5px;
  border: solid 1px rgb(170, 170, 170);
  button {
    border: none;
    border-radius: 5px;
    background-color: rgb(255, 156, 156);
    color: white;
    font-weight: 700;
    padding: auto;
  }
`;
