import React, { useState } from "react";
import styled from "styled-components";
import Todo from "./TodoItem";

type Props = {
  handleAddTodoItem: (title: string) => void;
};

const TodoForm = ({ handleAddTodoItem }: Props) => {
  const [title, setTitle] = useState<string>();
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    handleAddTodoItem(title!);
    setTitle("");
  };
  const handleChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  };
  return (
    <FormStyle onSubmit={(e) => handleSubmit(e)}>
      <label htmlFor="title">Add todo item</label>
      <input
        type="text"
        name="title"
        id="title"
        value={title || ""}
        onChange={(e) => handleChangeTitle(e)}
      />
      <button disabled={!title}>Save</button>
    </FormStyle>
  );
};

export default TodoForm;

const FormStyle = styled.form`
  label,
  input {
    display: block;
    width: 500px;
    margin: auto;
    padding: 0;
  }

  input {
    height: 1rem;
  }

  label {
    font-size: 20px;
    font-weight: 700;
  }

  button {
    text-align: center;
    display: block;
    margin: auto;
    margin-top: 1rem;
    margin-bottom: 1rem;
    font-size: 20px;
    background-color: rgb(56, 118, 217);
    color: white;
    font-weight: 700;
    border-radius: 5px;
  }

  button:disabled {
    background-color: rgb(150, 150, 150);
  }
`;
