import "./App.css";
import { Routes, Route, NavLink } from "react-router-dom";
import ColorBox from "./components/Lap_1/ColorBox";
import styled from "styled-components";
import TodoList from "./components/Lap_2_3/TodoList";

function App() {
  return (
    <>
      <NavStyle>
        <NavLink to="/lap-1">LAP 1</NavLink>
        <NavLink to="/lap-2-3">LAP 2-3</NavLink>
      </NavStyle>
      <Routes>
        <Route path="/lap-1" element={<ColorBox />} />
        <Route path="/lap-2-3" element={<TodoList />} />
      </Routes>
    </>
  );
}

export default App;

const NavStyle = styled.nav`
  display: flex;
  justify-content: center;
  align-items: center;
  a {
    margin: 1rem;
    font-weight: 900;
    text-decoration: none;
    border-radius: 10px;
    padding: 0.5rem;
    width: 100px;
    text-align: center;
  }
  .active {
    color: rgb(168, 50, 74);
    background-color: rgb(252, 217, 204);
  }
`;
