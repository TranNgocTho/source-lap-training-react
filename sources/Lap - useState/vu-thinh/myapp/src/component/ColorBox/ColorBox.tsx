import { useState } from "react";

const colorList = ['deeppink', 'green', 'yellow', 'black', 'blue'];

const initialState = (() => {
  let color = localStorage.getItem('color') || 'deeppink';
  return color;
})

const ColorBox = () => {
  const [color, setColor] = useState(initialState);
  localStorage.setItem('color', color);

  function handleBoxClick() {
    setColor(colorList[Math.floor(Math.random() * 5)]);
  }
  
  return <>
    <h1>lap 1</h1>
    <div style={{ backgroundColor: color, width: 100, height: 100 }} onClick={handleBoxClick} ></div>
  </>
}

export default ColorBox;

