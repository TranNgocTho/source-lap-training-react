import { useState } from "react";
import TodoList from "./TodoList";

const todoListInit = [
    { id: 1, title: "I love Easy Frontend! 😍 " },
    { id: 2, title: "We love Easy Frontend! 🥰 " },
    { id: 3, title: "They love Easy Frontend! 🚀 " },
];

const TodoForm = () => {
    const [todoList, setTodoList] = useState(todoListInit);
    const [item, setItem] = useState('');
    function onSubmit() {
        setTodoList([...todoList, { id: todoList.length > 0 ? todoList[todoList.length - 1].id + 1 : 1, title: item }])
    }

    return (      
        <div>
            <TodoList todoList={todoList} />
            <h1>lap 3</h1>
            <input type="text" onChange={e => setItem(e.target.value)} />
            <button type="submit" onClick={onSubmit}>
                submit
            </button>
        </div>
    );
};

export default TodoForm;
