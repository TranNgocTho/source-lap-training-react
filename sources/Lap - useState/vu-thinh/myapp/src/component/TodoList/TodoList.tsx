import { useEffect, useState } from "react";

// const todoListInit = [
//     { id: 1, title: "I love Easy Frontend! 😍 " },
//     { id: 2, title: "We love Easy Frontend! 🥰 " },
//     { id: 3, title: "They love Easy Frontend! 🚀 " },
// ];
type todoList = {
    id: number;
    title: string;
  };

type Props = {
    todoList: any;
};

const TodoList = (props: Props) => {
    const [todoList, setTodoList] = useState([]);

    useEffect(() => {
        setTodoList(props.todoList);
    }, [props.todoList]);

    function onTodoClick(id: number) {
        const newTodo = todoList.filter((item: todoList) => { return item.id !== id });
        setTodoList(newTodo);
    }

    return (
        <div>
            <h1>lap 2</h1>
            <ul>
                {todoList.map((item: todoList) => (
                    <li key={item.id} onClick={() => onTodoClick(item.id)}>
                        {item.title}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default TodoList;
