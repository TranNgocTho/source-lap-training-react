import './App.css';
import ColorBox from './component/ColorBox/ColorBox';
import TodoForm from './component/TodoList/TodoForm';
import TodoList from './component/TodoList/TodoList';

function App() {
  return (
    <div >
      <ColorBox />
      <TodoForm />
    </div>
  );
}

export default App;
