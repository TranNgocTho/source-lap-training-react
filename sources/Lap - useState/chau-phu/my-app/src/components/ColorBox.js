import React, { useState, useEffect, useMemo } from "react";

// Functional Component
// class ColorBox extends React.Component {

//     constructor(props) {
//         super(props)
//         console.log(props);
//         this.state = {
//             shape: "square",
//             color: props.color,
//         };
//         this.handleBoxClick = this.handleBoxClick.bind(this);
//     }    

//     handleBoxClick() {
//         this.setState({ color: "green" });
//     }

//     render() {
//         const { color } = this.state;
//         return (
//             <div>
//                 <div className="box" style={{ backgroundColor: color, width: 100, height: 100 }} onClick={this.handleBoxClick}>BBB</div>
//             </div>

//         )
//     }
// }
// export default ColorBox;

// Class Component
export default function ColorBox(props) {

    const colorList = useMemo(() => ['deeppink', 'green', 'yellow', 'black', 'blue'], []);

    const [color, setColor] = useState(() => {
        const localStorageData = localStorage.getItem('dataKey');
        if (localStorageData) {
            const newColor = localStorageData.replace(/"/g, '');
            const index = colorList.indexOf(newColor)
            return colorList[index];
        }
    });

    const handleBoxClick = () => {
        const randomNumber = Math.floor(Math.random() * 5);
        setColor(colorList[randomNumber]);
    };

    useEffect(() => {
        localStorage.setItem('dataKey', JSON.stringify(color));
    }, [color]);

    return (
        <div>
            <div className="box" style={{ backgroundColor: color, width: 100, height: 100 }} onClick={() => handleBoxClick()}>BBB</div>
        </div>
    )
}
