// import logo from './logo.svg';
import './App.css';
import Clock from './Clock';
import ColorBox from './components/ColorBox';

function App() {

	function formatDate(date) {
		return date.toLocaleDateString();
	}
	const comment = {
		date: new Date(),
		text: 'I hope you enjoy learning React!',
		author: {
			name: 'Hello Kitty',
			avatarUrl: 'https://placekitten.com/g/64/64',
		},
	};

	const account = {
		total: 3
	}

	function Avatar(props) {
		return (
			<div>
				<img src={props.user.avatarUrl} alt={props.user.name} />
			</div>
		)
	};

	function UserInfo(props) {
		return (
			<div>
				<Avatar user={props.user} />
				<div className="UserInfo-name">
					{props.user.name}
				</div>
			</div>
		)
	}

	function Comment(props) {
		return (
			<section>
				{/* <UserInfo user={props.author} />
				<div className="Comment-text">Text: {props.text}</div>
				<div className="Comment-date">{formatDate(props.date)}</div>
				<div className="Comment-sum">{sum(2, 3)}</div>
				<div className="Comment-sum">{withdraw(account, 2)}</div>
				<Clock date={props.date} /> */}
				{/* <ColorBox color="red" />
				<ColorBox color="blue" /> */}
				<ColorBox color="deeppink" />
			</section>
		)
	};

	function sum(a, b) {
		return a + b;
	}

	function withdraw(account, amount) {
		account.total -= amount;
	}

	const element = <Comment
		date={comment.date}
		text={comment.text}
		author={comment.author} />



	return (
		element
	);
}

export default App;
