function TodoList({ todos, onTodoClick }) {

    function removeItem(index) {
        onTodoClick(index);
    }
    return (
        <div>
            <ul>
                {
                    todos.map((item, index) => <li obj={item} key={index}>{item.title}
                        <button onClick={() => removeItem(index)}>remove item</button></li>)
                }
            </ul>

        </div>
    )
}

export default TodoList;
