import { useState } from "react";

function TodoForm({ addItemList }) {

    const [message, setMessage] = useState('');

    function handleSubmit(event) {
        event.preventDefault();
        addItemList(message);
    }

    const handleChange = event => {
        setMessage(event.target.value);
    };

    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <label>
                Name:
                <input type="text" name="name" onChange={handleChange} value={message} />
            </label>
            <input type="submit" value="Submit" />
        </form>
    )
};

export default TodoForm;