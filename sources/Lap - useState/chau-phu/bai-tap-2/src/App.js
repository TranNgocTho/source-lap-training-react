import './App.css';
import TodoList from './components/TodoList';
import React, { useState } from "react";
import TodoForm from './components/TodoForm';

function App() {

	const [todoList, setTodoList] = useState(() => {
		return [
			{ id: 1, title: "I love Easy Frontend! 😍 " },
			{ id: 2, title: "We love Easy Frontend! 🥰 " },
			{ id: 3, title: "They love Easy Frontend! 🚀 " },
		];
	});

	function handleTodoClick(index) {
		console.log("`{index}`", index);
		const updatedTodoList = [...todoList];
		updatedTodoList.splice(index, 1);
		setTodoList(updatedTodoList)
	}

	function addItem(value) {
		const maxId = Math.max.apply(Math, todoList.map(function (item) { return item.id; }));
		setTodoList(todoList => [...todoList, { id: maxId + 1, title: value }]);
	}

	return (
		<div>
			<TodoList todos={todoList} onTodoClick={handleTodoClick} />
			<TodoForm addItemList={addItem} />
		</div>
	)
}

export default App;
