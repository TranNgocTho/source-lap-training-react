import {BsFillCheckCircleFill} from 'react-icons/bs';
import {TbTrash} from 'react-icons/tb';
import {TaskType} from "../../types/task.type";

interface Task {
    task: TaskType,
    onDelete: (taskId: String) => void,
    onComplete: (taskId: String) => void,
}

export function Task({task, onDelete, onComplete}: Task) {
    return (
        <div
            className='w-full bg-black border border-gray-800 border-solid p-4 rounded-lg flex items-center justify-between'>
            <button className='w-[18px] h-[18px] bg-none border-none' onClick={() => onComplete(task.id)}>
                {task.isCompleted ? <BsFillCheckCircleFill className='w-full h-full text-purple-600'/> :
                    <div className='w-full h-full rounded-full border-solid border-blue-500 border-2'/>}
            </button>

            <p onClick={() => onComplete(task.id)}
               className={`text-sm ml-2 mr-auto text-white +${task.isCompleted ? "text-gray-600 line-through" : ""}`}>
                {task.title}
            </p>

            <button className='bg-none border-none text-gray-600' onClick={() => onDelete(task.id)}>
                <TbTrash size={20}/>
            </button>
        </div>
    )
}