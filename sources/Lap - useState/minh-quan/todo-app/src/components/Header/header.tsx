import todoLogo from "../../assets/todoLogo.svg";
import { AiOutlinePlusCircle } from "react-icons/ai";
import { useState } from "react";

interface HeaderType {
  handleAddTask: (taskId: string) => void;
}

export function Header({ handleAddTask }: HeaderType) {
  const [title, setTitle] = useState("");

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (title === "") {
      alert("Please enter task name");
      return;
    }
    handleAddTask(title);
    setTitle("");
  }

  function onChangeTitle(event: any) {
    setTitle(event.target.value);
  }

  return (
    <header className="flex justify-items-center relative bg-black items-center h-[200px]">
      <h1 className="w-full font-bold font-sans text-center text-white text-7xl">
        Todo App
      </h1>

      <form
        className="absolute h-[54px] bottom-[-27px] w-full flex gap-[8px] py-0 px-14"
        onSubmit={handleSubmit}
      >
        <input
          className="h-full flex-1 bg-gray-600 accent-white border border-solid border-black rounded-lg py-0 px-4 placeholder-gray-500"
          placeholder="Add a new task"
          type="text"
          onChange={onChangeTitle}
          value={title}
        />
        <button className="h-full py-0 px-4 bg-cyan-500 accent-white border-0 rounded-lg flex items-center gap-1.5 font-bold text-sm">
          Add Task <AiOutlinePlusCircle size={20} />
        </button>
      </form>
    </header>
  );
}
