import { TaskType } from "../../types/task.type";
import { Task } from "../Task/task";

interface ListType {
  tasks: Array<TaskType>;
  onDelete: (taskId: String) => void;
  onComplete: (taskId: String) => void;
}

export function List({ tasks, onDelete, onComplete }: ListType) {
  const tasksQuantity = tasks.length;
  const completedTasks = tasks.filter((task) => task.isCompleted).length;

  return (
    <section className="w-full max-w-[736px] my-0 mx-auto mt-[90px] py-0 px-4">
      <header className="flex items-center justify-between mb-6">
        <div className="flex items-center gap-2">
          <p className="text-blue-500 text-sm">Added tasks</p>
          <span className="bg-gray-800 text-gray-200 py-[3px] px-[9px] rounded-full text-xs font-bold">
            {tasksQuantity}
          </span>
        </div>

        <div className="flex items-center gap-2">
          <p className="text-purple-700 text-sm">Done tasks</p>
          <span className="bg-gray-800 text-gray-200 py-[3px] px-[9px] rounded-full text-xs font-bold">
            {completedTasks} of {tasksQuantity}
          </span>
        </div>
      </header>

      <div className="flex flex-col gap-3">
        {tasks.map((task) => (
          <Task
            key={task.id}
            task={task}
            onDelete={onDelete}
            onComplete={onComplete}
          />
        ))}
      </div>
    </section>
  );
}
