export interface TaskType {
    id: string,
    title: string,
    description: string,
    publishDate: Date,
    completeDate: string | null,
    isCompleted: boolean
}