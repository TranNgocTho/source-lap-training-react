import {useState} from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import {Header} from "./components/Header/header";
import {TaskType} from "./types/task.type";
import {List} from "./components/List/list";


interface Tasks extends Array<TaskType> {
}


function App() {
    const [tasks, setTasks] = useState<Tasks>(() => {
        return loadSavedTasks();
    });

    function setTasksAndSave(newTasks: Tasks) {
        setTasks(newTasks);
        console.log("Saving to localstorage")
        localStorage.setItem(import.meta.env.VITE_LOCAL_STORAGE_KEY || '', JSON.stringify(newTasks));
    }

    function loadSavedTasks() {
        const saved = localStorage.getItem(import.meta.env.VITE_LOCAL_STORAGE_KEY || '');
        if (saved) {
            return JSON.parse(saved);
        } else return [];
    }

    function addTask(taskTitle: string) {
        setTasksAndSave([...tasks, {
            id: crypto.randomUUID(),
            title: taskTitle,
            description: 'new task',
            publishDate: new Date(),
            isCompleted: false,
            completeDate: null
        }]);
    }

    function toggleTaskCompletedById(taskId: String) {
        const newTasks = tasks.map(task => {
            if (task.id === taskId) {
                return {
                    ...task,
                    isCompleted: !task.isCompleted
                }
            }
            return task;
        });
        setTasksAndSave(newTasks);
    }

    function deleteTaskById(taskId: String) {
        const newTasks = tasks.filter(task => task.id !== taskId);
        setTasksAndSave(newTasks);
    }

    return (
        <div className="App">
            <Header handleAddTask={addTask}/>
            <List
                tasks={tasks}
                onDelete={deleteTaskById}
                onComplete={toggleTaskCompletedById}
            />
        </div>
    )
}

export default App
