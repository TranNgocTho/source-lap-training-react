import React, { useState } from "react";

const colors = ["deeppink", "green", "yellow", "black", "blue"];

function ColorBox() {
  const [color, setColor] = useState("deeppink");

  return (
    <div
      style={{ background: color, width: 100, height: 100, color: "#fff" }}
      onClick={() => {
        setColor(colors[Math.trunc(Math.random() * (colors.length - 1))]);
      }}
    >
      {color}
    </div>
  );
}

export default ColorBox;
