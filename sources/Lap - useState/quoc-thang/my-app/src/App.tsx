import ColorBox from "./ColorBox/ColorBox";
import TodoForm from "./TodoList/TodoForm";

function App() {
  return (
    <div style={{ padding: 20 }}>
      <h3>Lab 1</h3>
      <ColorBox />
      <h3>Lab 2 + 3</h3>
      <TodoForm />
    </div>
  );
}

export default App;
