import './App.css';
import ColorBox from './component/ColorBox';
import TodoList from './component/TodoList';

function App() {
  return (
    <div className="App">
      <ColorBox />
      <br />
      <TodoList />
    </div>
  );
}

export default App;
