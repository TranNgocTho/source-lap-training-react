import { useState } from "react";

const color = ['deeppink', 'green', 'yellow', 'black', 'blue'];

const random = (() => {
    let temp = Math.trunc(Math.random() * 5)
    return color[temp];
})

const initialState = (() => {
    let temp = localStorage.getItem('color-box') || 'deeppink';
    return temp;
})

const ColorBox = () => {
    const [color, setColor] = useState(initialState);
    localStorage.setItem('color-box', color);
    function changeColor() {
        setColor(random);
    }
    return <>
        <h2 style={{ textAlign: "left" }}>lap 1</h2>
        <div onClick={changeColor} className='box' style={{ backgroundColor: color, width: 100, height: 100 }} ></div>
    </>
}

export default ColorBox;