import { useState } from "react";

const todoLists = [
    { id: 1, title: "I love Easy Frontend! 😍 " },
    { id: 2, title: "We love Easy Frontend! 🥰 " },
    { id: 3, title: "They love Easy Frontend! 🚀 " },
];

const TodoList = () => {
    const [todoList, setTodoList] = useState(todoLists);
    const [text, setText] = useState('');
    function removeTodo(index: number) {
        // Remember to clone into a new array
        const newTodoList = [...todoList];
        setTodoList(newTodoList.filter(item => { return item.id !== index }));
        console.log(index);
    }
    function addTodo() {
        setTodoList([...todoList, { id: todoList.length > 0 ? todoList[todoList.length - 1].id + 1 : 1, title: text }])
    }
    return (
        <div style={{ textAlign: "left" }}>
            <h2 style={{ textAlign: "left" }}>lap 2, lap 3</h2>
            <input type="text" onChange={e => setText(e.target.value)} />
            <input type="submit" onClick={addTodo} />
            <ul className="todo-list">
                {todoList.map((todo, index) => (
                    <li key={todo.id} onClick={() => removeTodo(todo.id)}>
                        {todo.title}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default TodoList;
