import "./App.css";
import TodoList from "./components/TodoList";
import ColorBox from "./components/ColorBox";

function App() {
  return (
    <div className="App">
      <ColorBox />
      <TodoList />
    </div>
  );
}

export default App;
