import './TodoList.css';
const ToDoList = (prop) => {
    function doSomething(e) {
         // do something here
        prop.onTodoClick(e);
    }
    const toDoList = prop.toDoList;
    return (
            <ul>
            {toDoList.map((e) => {
                return <li key={e.title}>{e.title}<button onClick={() =>doSomething(e)}>delete</button></li>;
            })}
            </ul>
      );
}


export default ToDoList;