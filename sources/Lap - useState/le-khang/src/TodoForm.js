import { useState } from "react";

function TodoForm(prop) {
    const [value, setValue] = useState("");
    const handleChange = (e) => {
        setValue(e.target.value);
    }
    const handleSubmit = () => {
        prop.submit(value);
        setValue("");
    }
    return (
        <div>
          <label>
            Name:
            <input type="text" value={value} onChange={handleChange} />
          </label>
          <input type="button" value="Submit" onClick={handleSubmit}/>
        </div>
    );
}

export default TodoForm;