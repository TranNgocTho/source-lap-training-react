import { useState } from 'react';
import './App.css';
import TodoForm from './TodoForm';
import TodoList from './TodoList';

const todoList = [
    { id: 1, title: "I love Easy Frontend! 😍 " },
    { id: 2, title: "We love Easy Frontend! 🥰 " },
    { id: 3, title: "They love Easy Frontend! 🚀 " },
  ];

function App() {
  const [list, setList] = useState([...todoList])
  
  const handleTodoClick = i => {
    let newList = [...list];
    const index = newList.indexOf(i);
    newList.splice(index , 1);
    setList(newList);
  };

  const handleSubmit = o => {
    let newList = [...list];
    var maxId = newList.reduce((arr, oId) => {
      return (arr = arr> oId.id ? arr : oId.id);
    });
    
    let newId = maxId + 1;
    newList.push({id : newId, title: o});
    setList(newList);
  }
  return (
    <div className="App">
      <TodoList toDoList={list} onTodoClick={handleTodoClick} />
      <TodoForm submit={handleSubmit}/>
    </div>
  );
}

export default App;
